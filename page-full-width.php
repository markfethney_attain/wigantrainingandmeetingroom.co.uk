<?php
/*
Template Name: Full Width Page
*/
?>

<?php get_header(); ?>

<div class="cta">
	<p class="text-center">
		<span class="cta-text">Call us on: 0845 299 3412</span><a class="btn btn-green" href="#form">Enquire Now</a>
	</p> 
</div>

<div class="intro">
	<div class="container">
		<p class="col-md-6 col-md-offset-3">Situated in the heart of the town, the Training & Meeting Room offers groups the opportunity to meet, learn or network in peaceful, private surroundings yet remain in the centre of Wigan’s bustling town.</p>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 content-col find-us">
			<div>
				<img src="/wp-content/uploads/2017/08/icon-map-marker-75@2x.png" alt="Find Us" width="75">
				<h2 class="heading">Find Us</h2>
			</div>
			<p>Our entrance is found in the oldest shopping street in historic Wigan, the <strong style="color: #ffffff;">Royal Arcade</strong> which boasts an array of photographs along it’s walls & ceilings depicting Wigan’s heritage. A quirky indoor ‘street’ brimming with designer labels, afternoon tea rooms & salons providing customers with the very best in hair, beauty & aesthetic services. Above the shops you will find our rooms. Fully equipped for training & adaptable to suit all types of meetings.</p>
			<p class="find-us-tag">We can accommodate up to 12 people comfortably. </p>
		</div>
		<div class="col-md-6 content-col equipment">
			<div class="text-center">
				<img src="/wp-content/uploads/2017/08/icon-shake-68@2x.png" alt="Equipment" width="68">
				<h2 class="heading">Equipment</h2>
				<ul class="two-col text-left">
					<li>Television</li>
					<li>Wipe board & pens</li>
					<li>Tables</li>
					<li>Chairs</li>
					<li>Therapy couch <small>(ideal for all aspects of beauty/aesthetic training)</small></li>
					<li>Serviced reception</li>
					<li>Wifi <small>(free)</small></li>
					<li>Onsite catering facilities <small>(additional cost, ask about our packages)</small></li>
					<li> Pens & note books <small>(additional cost, price on request)</small></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 content-col quote">
			<section>
				<p>Your smile is your logo, your personality is your business card, how you leave others feeling after an experience with you becomes your trademark</p>
			</section>
		</div>
		<div class="col-md-6 content-col price-list">
			<h2 class="heading text-center">Price List</h2>

			<div class="price-mob">
				<ul>
					<li>Full day 9am-5pm: <strong>£99</strong></li>
					<li>Half day 9am-1pm or 1pm-5pm: <strong>£59</strong></li>
					<li>Per hour: <strong>£16.99 ph</strong> or </li>
					or
					<li><strong>£20 ph</strong></ <small>(Outside of normal working hours)</small></li>
				</ul>
				<h3 class="text-center" style="width: 100%;">Discounts Apply:</h3>
				<ul>
					<li>Book 2 or more consecutive days <strong>10% Discount</strong></li>
					<li>Book 5 or more consecutive days <strong>20% Discount</strong></li>
					<li>Regular meetings (weekly/monthly) <strong>10% Discount</strong></li>
				</ul>
			</div>

			<div class="price-desk">
				<div class="row">
					<div class="col-md-6 text-right">
						<p>Full day 9am-5pm</p>
						<p>Half day 9am-1pm or 1pm-5pm</p>
						<p>Per hour</p>
					</div>
					<div class="col-md-6 text-left">
						<p>£99</p>
						<p>£59</p>
						<p>£16.99 ph</p>
						<p>£20 ph <small>(Outside of normal working hours)</small></p>
					</div>
				</div>
				<h3 class="text-center" style="width: 100%;">Discounts Apply:</h3>
				<div class="row">
					<div class="col-md-6 text-right">
						<p>Book 2 or more consecutive days</p>
						<p>Book 5 or more consecutive days</p>
						<p>Regular meetings (weekly/monthly)</p>
					</div>
					<div class="col-md-6 text-left">
						<p>10% Discount</p>
						<p>20% Discount</p>
						<p>10% Discount</p>
					</div>
				</div>
			</div>

			<p class="text-center"><small>(Payment must be made in advance)</small></p>
		</div>
	</div>
</div>

<div class="spacer"></div>

<div class="container" id="form">
	<div class="text-center">
		<h2>Enquire Now</h2>
		<img src="/wp-content/uploads/2017/08/hr-shadow.png" class="hr-shadow">
		<p>Simply fill out the form below and we’ll get in touch to arrange an appointment</p>
	</div>
	<?php echo do_shortcode('[contact-form-7 id="7" title="Contact form 1"]'); ?>
</div>

<div class="spacer"></div>

<div class="gallery-holder">
	<div class="container">
		<div class="row">
			<div class="gallery-pad text-center">
				<h2>Gallery</h2>
				<p><strong>Our training/meeting room is suitable for a variety of uses</strong> <br> We can arrange for treatment couches/tables/chairs etc. to meet your specific requirements</p>
				<ul class="gallery-slick">
					<li><a href="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-1.jpg"><img src="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-1-220x300.jpg"></a></li>
					<li><a href="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-2.jpg"><img src="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-2-236x300.jpg"></a></li>
					<li><a href="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-3.jpg"><img src="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-3-205x300.jpg"></a></li>
					<li><a href="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-4.jpg"><img src="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-4-208x300.jpg"></a></li>
					<li><a href="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-5.jpg"><img src="/wp-content/uploads/2017/08/wigan-meeting-room-gallery-5-222x300.jpg"></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {

	// dynamic changes for 4 col heights
	function checkPosition() {
	    if (window.matchMedia('(min-width: 992px)').matches) {

	      var maxHeight = 0;

	      $(".content-col").each(function(){
	         if ($(this).outerHeight() > maxHeight) { maxHeight = $(this).outerHeight(); }
	      });

	      $(".content-col").outerHeight(maxHeight);

	  }
	}
	$(window).resize(function() {
	   checkPosition();
	});
	$(document).ready(function() {
	   checkPosition();
	});

	//slider
	$('.gallery-slick').slick({
	  dots: true,
	  arrows: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: 'unslick'
	    },
	  ]
	});

	//popup
	$('.gallery-slick').magnificPopup({
	  delegate: 'a', // child items selector, by clicking on it popup will open
	  type: 'image'
	  // other options
	});

	$( function() {
	  $( ".datepicker" ).datepicker();
	} );

	//store the ref_id elsewhere and remove the value
	var idSetter = $('input[name="ref_no"]');
	var ref_no = idSetter.val();
	idSetter.attr('ref_id_store', '');
	idSetter.attr('ref_id_store', ref_no);
	idSetter.val('');

	//remove ref_no if checkbox not checked
	$('input[name="getNews"]').click(function() {
	  var form = $(this).closest('form');
	  var _idSetter = form.find('input[name="ref_no"]');

	  if(this.checked) {
	    _idSetter.val(ref_no);
	  } else {
	    _idSetter.val('');
	  }
	});


});
</script>

<?php get_footer(); ?>