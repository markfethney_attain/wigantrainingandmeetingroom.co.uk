<?php
/*
Template Name: Confirmation
*/
?>

<?php get_header(); ?>

<br> <br>

<div class="container text-center">
	<p>Please press the below button if you want to be kept up to date with all the latest news and events from The Training & Meeting Room</p>
	<?php echo do_shortcode('[contact-form-7 id="17" title="confirmation"]'); ?>
</div>


<?php get_footer(); ?>