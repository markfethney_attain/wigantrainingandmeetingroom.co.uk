
        <footer>
            <div id="inner-footer" class="vertical-nav">
                <div class="container">
                    <div class="row">
                        <?php dynamic_sidebar('footer1'); ?>

                        <div class="col-xs-12 text-center">
                                    <footer>
                                        <p>Office 2, 2nd floor WN1 1QH</p>
                                        <p>T: 0845 299 3412</p>
                                        <div class="social-icons">
                                            <a href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i><span class="sr-only">Facebook</span></a>
                                            <a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i><span class="sr-only">Twitter</span></a>
                                            <a href="#" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i><span class="sr-only">LinkedIn</span></a>
                                        </div>
                                        <p style="color:#636e82;">© Meeting&TrainingRooms 2017</p>
                                    </footer>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

                
           </div>
        </div>
            

    </div>

	<?php wp_footer(); // js scripts are inserted using this function ?>

</body>

</html>
