<!doctype html>  

<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Wigan Training and Meeting Room">
	<meta itemprop="description" content="<?php wp_title(''); ?> - ">
	<meta itemprop="image" content="http://www.wigantrainingandmeetingroom.co.uk/wp-content/uploads/2017/08/WTMR-open-graph.jpg">

	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="">
	<meta name="twitter:title" content="Wigan Training and Meeting Room">
	<meta name="twitter:description" content="">
	<meta name="twitter:creator" content="@theattainteam">
	<meta name="twitter:image:src" content="http://www.wigantrainingandmeetingroom.co.uk/wp-content/uploads/2017/08/WTMR-open-graph.jpg">

	<!-- Open Graph data -->
	<meta property="og:title" content="Wigan Training and Meeting Room" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="http://www.wigantrainingandmeetingroom.co.uk/wp-content/uploads/2017/08/WTMR-open-graph.jpg" />
	<meta property="og:description" content="" />
	<meta property="og:site_name" content="Wigan Training and Meeting Room" />

	<?php wp_head(); ?>
</head>
	
<body <?php body_class(); ?>>

	<div id="content-wrapper">

		<header>
			<div class="container">
				<div class="col-md-12 site-header">
					<h1 class="text-center">The Training &amp; Meeting Room</h1>
					<hr class="blue sm">
					<p class="text-center sub-header">The place to meet in Wigan town centre</p>
				</div>
			</div>
		</header>

